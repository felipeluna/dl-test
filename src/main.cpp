// #include <iostream>
// #include <sqlite3.h>

// using namespace std;

// int main()
// {
// 	// Pointer to SQLite connection
// 	sqlite3 *db;

// 	// Save the connection result
// 	int exit = 0;
// 	exit = sqlite3_open("example.db", &db);

// 	// Test if there was an error
// 	if (exit)
// 	{

// 		cout << "DB Open Error: " << sqlite3_errmsg(db) << endl;
// 	}
// 	else
// 	{

// 		cout << "Opened Database Successfully!" << endl;
// 	}

// 	// Close the connection
// 	sqlite3_close(db);

// 	return (0);
// }

// #include <iostream>
// #include <SDL2/SDL.h>
// using namespace std;
// int main()
// {
// 	cout << "Hello World!";
// 	SDL_Init(SDL_INIT_VIDEO);
// 	return 0;
// }

// Example program:
// SDL_DropEvent usage

// #include <SDL2/SDL.h>
// #include <iostream>
// #include <string>
// using namespace std;

// int main(int argc, char *argv[])
// {
// 	SDL_bool done;
// 	SDL_Window *window;
// 	SDL_Event event;	   // Declare event handle
// 	char *dropped_filedir; // Pointer for directory of dropped file
// 	uint32_t d_filetype;
// 	SDL_Init(SDL_INIT_VIDEO); // SDL2 initialization

// 	window = SDL_CreateWindow( // Create a window
// 		"SDL_DropEvent usage, please drop the file on window",
// 		SDL_WINDOWPOS_CENTERED,
// 		SDL_WINDOWPOS_CENTERED,
// 		640,
// 		480,
// 		SDL_WINDOW_OPENGL);

// 	// Check that the window was successfully made
// 	if (window == NULL)
// 	{
// 		// In the event that the window could not be made...
// 		SDL_Log("Could not create window: %s", SDL_GetError());
// 		SDL_Quit();
// 		return 1;
// 	}

// 	SDL_EventState(SDL_DROPFILE, SDL_ENABLE);
// 	SDL_EventState(SDL_SYSWMEVENT, SDL_ENABLE);

// 	done = SDL_FALSE;
// 	while (!done)
// 	{ // Program loop
// 		while (!done && SDL_PollEvent(&event))
// 		{
// 			switch (event.type)
// 			{
// 			case (SDL_QUIT):
// 			{ // In case of exit
// 				done = SDL_TRUE;
// 				break;
// 			}

// 			case (SDL_SYSWMEVENT):
// 			{
// 				cout << "a";
// 			}
// 			case (SDL_DROPTEXT):
// 			{ // In case if dropped file
// 				dropped_filedir = event.drop.file;
// 				d_filetype = event.drop.type;
// 				// Shows directory of dropped file
// 				SDL_ShowSimpleMessageBox(
// 					SDL_MESSAGEBOX_INFORMATION,
// 					"File dropped on window",
// 					"dropped_filedir",
// 					window);
// 				SDL_free(dropped_filedir); // Free dropped_filedir memory
// 				break;
// 			}
// 			}
// 		}
// 		SDL_Delay(0);
// 	}

// 	SDL_DestroyWindow(window); // Close and destroy the window

// 	SDL_Quit(); // Clean up
// 	return 0;
// }

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

int main(int argc, char *argv[])
{
	SDL_Window *window;
	SDL_SysWMinfo info;

	SDL_Init(0);

	window = SDL_CreateWindow("", 0, 0, 0, 0, SDL_WINDOW_HIDDEN);

	SDL_VERSION(&info.version); /* initialize info structure with SDL version info */

	if (SDL_GetWindowWMInfo(window, &info))
	{ /* the call returns true on success */
		/* success */
		const char *subsystem = "an unknown system!";
		switch (info.subsystem)
		{
		case SDL_SYSWM_UNKNOWN:
			break;
		case SDL_SYSWM_WINDOWS:
			subsystem = "Microsoft Windows(TM)";
			break;
		case SDL_SYSWM_X11:
			subsystem = "X Window System";
			break;
#if SDL_VERSION_ATLEAST(2, 0, 3)
		case SDL_SYSWM_WINRT:
			subsystem = "WinRT";
			break;
#endif
		case SDL_SYSWM_DIRECTFB:
			subsystem = "DirectFB";
			break;
		case SDL_SYSWM_COCOA:
			subsystem = "Apple OS X";
			break;
		case SDL_SYSWM_UIKIT:
			subsystem = "UIKit";
			break;
#if SDL_VERSION_ATLEAST(2, 0, 2)
		case SDL_SYSWM_WAYLAND:
			subsystem = "Wayland";
			break;
		case SDL_SYSWM_MIR:
			subsystem = "Mir";
			break;
#endif
#if SDL_VERSION_ATLEAST(2, 0, 4)
		case SDL_SYSWM_ANDROID:
			subsystem = "Android";
			break;
#endif
#if SDL_VERSION_ATLEAST(2, 0, 5)
		case SDL_SYSWM_VIVANTE:
			subsystem = "Vivante";
			break;
#endif
		}

		SDL_Log("This program is running SDL version %d.%d.%d on %s",
				(int)info.version.major,
				(int)info.version.minor,
				(int)info.version.patch,
				subsystem);
	}
	else
	{
		/* call failed */
		SDL_LogError(SDL_LOG_CATEGORY_ERROR, "Couldn't get window information: %s", SDL_GetError());
	}

	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}